declare namespace Express {
  interface Request {
    user?: string,
    fileValidationError?: Error | string
  }
}
