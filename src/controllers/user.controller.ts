import UserService from '../services/user.service'
import status from 'http-status'
import { NextFunction, Request, Response } from 'express'

export class UserController {

  async SignUp(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await UserService.SignUp(req, res)
      return res.status(status.OK).json({
        message: 'User created successfully',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async Login(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await UserService.Login(req, res)
      return res.status(status.OK).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async refreshToken(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await UserService.refreshToken(req, res)
      return res.status(status.OK).json({
        status: 'success',
        token: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async Profile(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await UserService.Profile(req, res)
      return res.status(status.OK).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async profilePicture(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await UserService.profilePicture(req, res)
      return res.status(status.OK).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }
}
export default new UserController()
