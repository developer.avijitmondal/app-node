import CategoryService from '../services/category.service'
import status from 'http-status'
import { NextFunction, Request, Response } from 'express'

export class CategoryController {

  async createCategory(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await CategoryService.createCategory(req)
      return res.status(status.CREATED).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async getCategory(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await CategoryService.getCategory(req)
      return res.status(status.OK).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }
}
export default new CategoryController()
