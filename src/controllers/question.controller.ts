import QuestionService from '../services/questions.service'
import status from 'http-status'
import { NextFunction, Request, Response } from 'express'

export class QuestionController {

  async createQuestion(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await QuestionService.createQuestion(req, res)
      return res.status(status.CREATED).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async getCategory(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await QuestionService.getQuestion(req)
      return res.status(status.OK).json({
        status: 'success',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }

  async bulkQuestion(req: Request, res: Response, next: NextFunction): Promise<Response<Record<string, unknown>> | void> {
    try {
      const result = await QuestionService.bulkQuestion(req, res)
      return res.status(status.CREATED).json({
        status: 'success',
        message: 'File uploaded and questions created successfully',
        data: result
      })
    } catch (error) {
      return next(error)
    }
  }
}
export default new QuestionController()
