import mongoose, { ConnectOptions } from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

// Get the MongoDB URI from environment variables
const uri = process.env.MONGODB_URI || '';

export async function connectDatabase() {
    try {
        // const options: ConnectOptions = {
        //     useNewUrlParser: true,
        //     useUnifiedTopology: true,
        // };
        await mongoose.connect(uri);
        console.log('Connected to MongoDB Atlas 👽');
    } catch (error) {
        console.error('Error connecting to MongoDB:', error);
        throw error;
    }
}

export { mongoose };
