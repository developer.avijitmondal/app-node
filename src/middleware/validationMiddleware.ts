import { Request, Response, NextFunction } from 'express';
import Joi, { ObjectSchema } from 'joi';
import httpStatus from 'http-status';

// Define Joi schema for different routes
const schemas = {
  createCategory: Joi.object({
    name: Joi.string().required().messages({
      'any.required': 'Name is required',
      'string.base': 'Name must be a string',
    }),
  }),
  createQuestion: Joi.object({
    text: Joi.string().required().messages({
      'any.required': 'Text is required',
      'string.base': 'Text must be a string',
    }),
    categories: Joi.array().items(Joi.string().required()).min(1).required().messages({
      'any.required': 'Categories are required',
      'array.min': 'At least one category must be selected',
      'string.base': 'Category IDs must be strings',
    }),
  }),
  createUser: Joi.object({
    name: Joi.string().required().messages({
      'any.required': 'Name is required',
      'string.base': 'Name must be a string',
    }),
    email: Joi.string().email().required().messages({
      'any.required': 'Email is required',
      'string.email': 'Email must be a valid email address',
    }),
    password: Joi.string().required().messages({
      'any.required': 'Password is required',
      'string.base': 'Password must be a string',
    }),
  }),

  loginUser: Joi.object({
    email: Joi.string().email().required().messages({
      'any.required': 'Email is required',
      'string.email': 'Email must be a valid email address',
    }),
    password: Joi.string().required().messages({
      'any.required': 'Password is required',
      'string.base': 'Password must be a string',
    }),
  }),
  refreshToken: Joi.object({
    refreshToken: Joi.string().required().messages({
      'any.required': 'Refresh token is required',
      'string.base': 'Refresh token must be a string',
    }),
  }),

};

// Middleware function to validate requests
export const validateRequest = (schema: ObjectSchema) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body, { abortEarly: false });
    if (error) {
      return res.status(httpStatus.BAD_REQUEST).json({ errors: error.details.map(detail => detail.message) });
    }
    next();
  };
};

export default schemas;