// src/middleware/auth.ts
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import httpStatus from 'http-status';

dotenv.config();

export interface CustomJwtPayload extends jwt.JwtPayload {
  _id: string;
}

const auth = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('Authorization')?.replace('Bearer ', '');

  if (!token) {
    return res.status(httpStatus.UNAUTHORIZED).json({ status: 'error', message: 'Access denied. No token provided.' });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET as string) as CustomJwtPayload;
    req.user = decoded.id; // Set req.user to the _id from decoded JWT payload
    next();
  } catch (ex) {
    res.status(httpStatus.BAD_REQUEST).json({ status: 'error', message: 'Invalid token.' });
  }
};

export default auth;
