import { Request, Response, NextFunction } from 'express';

// Extend the Response interface to include success and error methods
interface CustomResponse extends Response {
  success(data: any): void;
  error(error: any, statusCode?: number): void;
}

const responseMiddleware = (req: Request, res: CustomResponse, next: NextFunction) => {
  res.success = (data: any) => {
    res.status(200).json({
      status: 'success',
      data
    });
  };

  res.error = (error: any, statusCode: number = 500) => {
    res.status(statusCode).json({
      status: 'error',
      message: error.message || 'An error occurred',
      error
    });
  };

  next();
};

export default responseMiddleware;
