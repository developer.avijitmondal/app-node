import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { UserDocument } from '../models/user';

dotenv.config();

export const constants = Object.freeze({
  ROUTE: '/api/',
  ROUTE_VERSION: 'v1',
})

export const generateRefreshToken = (user: UserDocument) => {
  // console.log(user)
  return jwt.sign({ id: user._id }, process.env.REFRESH_TOKEN_SECRET as string, { expiresIn: process.env.REFRESH_TOKEN_EXPIRATION });
}