import express, { NextFunction, Request, Response } from 'express';
import { connectDatabase } from './config/db';
// import userRoutes from './routes/user.routes';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';
import bodyParser from 'body-parser';
// import categoryRoutes from './routes/category.routes';
// import questionRoutes from './routes/question.routes';
import helmet from 'helmet'
import BaseRouter from './routes/index'

// Create Express app
const app = express();

app.use(bodyParser.json());

// Middleware
app.use(express.json());
connectDatabase();

// Use Helmet middleware
app.use(helmet());

// Routes
// app.get('/', (req: Request, res: Response) => {
//     res.json('Hello World!');
// });

// Routes
// app.use('/users', userRoutes);
// app.use('/categories', categoryRoutes);
// app.use('/questions', questionRoutes);

// Add APIs
app.use('/', BaseRouter)


// Serve Swagger documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Error handling middleware
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    console.error(err.stack); // Log the error stack trace
  
    // Check if the error is a Joi validation error
    if (err.isJoi) {
      return res.status(400).json({
        status: 'error',
        message: 'Validation error',
        errors: err.details.map((detail: any) => detail.message),
      });
    }
  
    // Default error handling
    res.status(500).json({
      status: 'error',
      message: err.message || 'Internal Server Error',
    });
  });  

// Start server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT} 🔒`);
});
