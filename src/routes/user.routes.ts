import express, { Router, Request } from 'express';
import multer from 'multer';
import path from 'path';
import auth from '../middleware/auth';
import { v4 as uuidv4 } from 'uuid'; // Import uuid v4 generator
import { validateRequest } from '../middleware/validationMiddleware';
import schemas from '../middleware/validationMiddleware'; // Import schemas for specific routes
import UserController from '../controllers/user.controller';

const router: Router = express.Router();

// Multer setup for file upload with file type validation (images only)
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Set your destination folder for uploaded images
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = uuidv4();
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
  }
});

const fileFilter = (req: Request, file: Express.Multer.File, cb: multer.FileFilterCallback) => {
  // Check if file type is an image
  if (file.mimetype.startsWith('image/')) {
    cb(null, true);
  } else {
    // cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE', file.fieldname));
    const error = new multer.MulterError('LIMIT_UNEXPECTED_FILE', file.fieldname);
    error.message = 'File type is invalid. Please upload only image files.';
    cb(error);
  }
};

const upload = multer({ storage, fileFilter });

router
  .route('/profile-picture')
  .post(auth, upload.single('profilePicture'), UserController.profilePicture)

router
  .route('/profile')
  .get(auth, UserController.Profile)

// POST /api/login - User login route with specified response format
router
  .route('/login')
  .post(validateRequest(schemas.loginUser), UserController.Login)

// Define routes
router
  .route('/signup')
  .post(validateRequest(schemas.createUser), UserController.SignUp)

router
  .route('/refresh-token')
  .post(auth, validateRequest(schemas.refreshToken), UserController.refreshToken)
export default router;
