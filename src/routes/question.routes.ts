import { Router, Request } from 'express';
// import Question from '../models/question';
import multer from 'multer';
import schemas, { validateRequest } from '../middleware/validationMiddleware';
import CategoryController from '../controllers/question.controller';

const router = Router();

router
  .route('/')
  .get(CategoryController.getCategory)

router
  .route('/')
  .post(validateRequest(schemas.createQuestion), CategoryController.createQuestion)

// Multer setup for file upload with file type validation
const upload = multer({
  dest: 'uploads/',
  fileFilter: (req: Request, file: Express.Multer.File, cb) => {
    if (file.mimetype !== 'text/csv') {
      const error = new multer.MulterError('LIMIT_UNEXPECTED_FILE', file.fieldname);
      error.message = 'File type is invalid. Please upload only CSV files.';
      cb(error);
    } else {
      cb(null, true);
    }
  },
});

router
  .route('/bulk')
  .post(upload.single('csvFile'), CategoryController.bulkQuestion)

export default router;
