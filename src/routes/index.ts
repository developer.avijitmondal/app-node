import { Router } from 'express'
import { constants } from '../helpers/helpers'
import categoryRoutes from './category.routes'
import questionRoutes from './question.routes'
import userRoutes from './user.routes'

const ROUTE_PREFIX = constants.ROUTE + constants.ROUTE_VERSION
const router = Router()
console.log(ROUTE_PREFIX)
// API Routes
router.use(ROUTE_PREFIX + '/categories', categoryRoutes)
router.use(ROUTE_PREFIX + '/questions', questionRoutes)
router.use(ROUTE_PREFIX + '/users', userRoutes)

// Export the base-router
export default router
