import { Router } from 'express';
// import Category from '../models/category';
import { validateRequest } from '../middleware/validationMiddleware';
import schemas from '../middleware/validationMiddleware'; // Import schemas for specific routes
import CategoryController from '../controllers/category.controller'
const router = Router();

router
  .route('/')
  .get(CategoryController.getCategory)

router
  .route('/')
  .post(validateRequest(schemas.createCategory),  CategoryController.createCategory)

export default router;
