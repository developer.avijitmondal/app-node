import { Request, Response, query } from 'express';
import httpStatus from 'http-status';
import User from '../models/user';
import bcrypt from 'bcryptjs';
import { generateToken } from '../utils/jwt';
import mongoose from 'mongoose';
import { generateRefreshToken } from '../helpers/helpers';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { CustomJwtPayload } from '../middleware/auth';

dotenv.config();

export class UserRepository {

  async Profile(req: Request, res: Response) {
    // req.user should now contain the _id of the authenticated user
    console.log(req.user)
    if (!req.user) {
      return res.status(httpStatus.UNAUTHORIZED).json({ status: 'error', message: 'Unauthorized' });
    }
    console.log(req.user)
    // Fetch the user from the database using aggregation
    const users = await User.aggregate([
      { $match: { _id: new mongoose.Types.ObjectId(req.user) } },
      { $project: { password: 0 } } // Exclude password from the result
    ]);

    if (users.length === 0) {
      return res.status(httpStatus.NOT_FOUND).json({ status: 'error', message: 'User not found' });
    }

    return users[0];
  }

  async refreshToken(req: Request, res: Response) {
    const refreshToken = req.body.refreshToken;

    if (!refreshToken) {
      return res.status(401).json({ message: 'Refresh token is required' });
    }

    try {
      const decoded = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET as string) as CustomJwtPayload;
      // const user = User.find(u => u.id === decoded.userId);
      const users = await User.aggregate([
        { $match: { _id: new mongoose.Types.ObjectId(decoded.id)  } },
        { $limit: 1 }
      ]);
      console.log(decoded.id)
      console.log(users)
      // const decoded = jwt.verify(refreshToken, process.env.JWT_SECRET as string) as unknown as CustomJwtPayload;
      // req.user = decoded.id; // Set req.user to the _id from decoded JWT payload

      if (users.length === 0) {
        return res.status(httpStatus.NOT_FOUND).json({ status: 'error', message: 'User not found' });
      }

      const user = users[0];

      if (!user) {
        return res.status(401).json({ message: 'Invalid refresh token' });
      }

      return generateToken(user);
      // res.json({ token: newToken });
    } catch (err) {
      console.error('Error refreshing token:', err);
      res.status(403).json({ message: 'Invalid refresh token' });
    }

  }

  async Login(req: Request, res: Response) {
    const { email, password } = req.body;
    // Use aggregation to check if user with given email exists
    const users = await User.aggregate([
      { $match: { email } },
      { $limit: 1 }
    ]);

    if (users.length === 0) {
      return res.status(httpStatus.NOT_FOUND).json({ status: 'error', message: 'User not found' });
    }

    const user = users[0];

    // Verify password
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(httpStatus.UNAUTHORIZED).json({ status: 'error', message: 'Invalid password' });
    }

    // Generate JWT token
    // return generateToken(user);
    // const refreshToken = generateRefreshToken(user);
    return {
      token: generateToken(user),
      refreshToken: generateRefreshToken(user)
    }
    // return []
  }

  async SignUp(req: Request, res: Response) {
    const { name, email, password } = req.body;
    // Check if user with given email already exists
    const existingUser = await User.aggregate([
      { $match: { email } },
      { $limit: 1 }
    ]);
    console.log(existingUser)
    if (existingUser.length > 0) {
      return res.status(httpStatus.BAD_REQUEST).json({ message: 'User already exists with this email' });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create new user
    const newUser = new User({
      name,
      email,
      password: hashedPassword
    });

    // Save user to database
    return await newUser.save();
  }

}

export default new UserRepository()
