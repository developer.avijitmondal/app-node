import { Request, Response, query } from 'express';
import httpStatus from 'http-status';
import mongoose, { ObjectId, PipelineStage } from 'mongoose';
import Question, { QuestionDocument } from '../models/question'; // Adjust the import path based on your project structure
import Category from '../models/category'

export class QuestionRepository {

  async createQuestion(req: Request, res: Response) {
    const { text, categories } = req.body;

    // Validate categories if needed
    const validCategories = await Category.find({ '_id': { $in: categories } });
    if (validCategories.length !== categories.length) {
      return res.status(httpStatus.BAD_REQUEST).json({
        status: 'error',
        message: 'One or more categories are invalid'
      });
    }

    const question = new Question({ text, categories });
    return await question.save();
  }

  async getQuestion(req: Request, page: number = 1, pageSize: number = 10) {

    const getPage = Number(req?.query?.page) ? Number(req?.query?.page) : page
    const getPageSize = Number(req?.query?.size)  ? Number(req?.query?.size) : pageSize

    const skip = (getPage - 1) * getPageSize;

    return await Category.aggregate([
      {
        $lookup: {
          from: 'questions',
          localField: '_id',
          foreignField: 'categories',
          as: 'questions'
        }
      },
      {
        $project: {
          _id: 1,
          name: 1,
          questions: {
            $map: {
              input: '$questions',
              as: 'question',
              in: {
                _id: '$$question._id',
                text: '$$question.text'
              }
            }
          }
        }
      },
      {
        $skip: skip
      },
      {
        $limit: getPageSize
      }
    ]);
  }
}

export default new QuestionRepository()
