import { Request, query } from 'express';
import httpStatus from 'http-status';
import mongoose, { ObjectId, PipelineStage } from 'mongoose';
import Category from '../models/category';

export class CategoryRepository {

  async createCategory(req: Request) {
    const { name } = req.body;
    const category = new Category({ name });
    return await category.save();
  }

  async getCategory(req: Request, page: number = 1, pageSize : number = 10) {
    
    const getPage = Number(req?.query?.page) ? Number(req?.query?.page) : page
    const getPageSize = Number(req?.query?.size)  ? Number(req?.query?.size) : pageSize

    const skip = (getPage - 1) * getPageSize;

    return await Category.aggregate([
      {
        $match: {}
      },
      {
        $skip: skip
      },
      {
        $limit: getPageSize
      }
    ]);
  }
}

export default new CategoryRepository()
