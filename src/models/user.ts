// models/user.ts

import mongoose, { Document, Schema } from 'mongoose';

export interface UserDocument extends Document {
  name: string;
  email: string;
  password: string;
  profilePicture?: string; // Optional field for profile picture
  createdAt: Date;
}

const userSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  profilePicture: { type: String }
});

const User = mongoose.model<UserDocument>('User', userSchema);

export default User;