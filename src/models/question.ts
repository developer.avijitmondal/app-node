import mongoose, { Document, Schema } from 'mongoose';

// Define interface for Question document
export interface QuestionDocument extends Document {
  text: string;
  categories: mongoose.Types.ObjectId[]; // Assuming categories are ObjectId array
}

// Define schema for Question
const questionSchema = new Schema({
  text: { type: String, required: true },
  categories: [{ type: Schema.Types.ObjectId, ref: 'Category', required: true }]
});

// Create and export Mongoose model
const Question = mongoose.model<QuestionDocument>('Question', questionSchema);
export default Question;
