import { Request } from 'express';
import httpStatus from 'http-status';
import mongoose, { Types } from 'mongoose';
import CategoryRepository from '../repository/category.repository';

export class CategoryService {

  async createCategory(req: Request) {
    return await CategoryRepository.createCategory(req)
  }

  async getCategory(req: Request) {
    return await CategoryRepository.getCategory(req)
  }
}

export default new CategoryService()