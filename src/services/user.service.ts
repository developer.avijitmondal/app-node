import { Request, Response } from 'express';
import httpStatus from 'http-status';
import mongoose, { Types } from 'mongoose';
import UserRepository from '../repository/user.repository';
import User from '../models/user';

export class UserService {

  async SignUp(req: Request, res: Response) {
    return await UserRepository.SignUp(req, res)
  }

  async Login(req: Request, res: Response) {
    return await UserRepository.Login(req, res)
  }

  async refreshToken(req: Request, res: Response) {
    return await UserRepository.refreshToken(req, res)
  }

  async Profile(req: Request, res: Response) {
    return await UserRepository.Profile(req, res)
  }

  async profilePicture(req: Request, res: Response) {
    if (!req.user) {
      return res.status(401).json({ status: 'error', message: 'Unauthorized' });
    }

    if (!req.file) {
      return res.status(400).json({ status: 'error', message: 'No file uploaded' });
    }

    const profilePicture = req.file.filename;

    // Start a Mongoose session for transaction
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      // Update the user's profile picture
      const updatedUser = await User.findOneAndUpdate(
        { _id: req.user },
        { $set: { profilePicture, name: req.body.name } },
        { new: true, projection: { password: 0 }, session }
      );

      // Commit the transaction
      await session.commitTransaction();

      // End the session
      session.endSession();

      // Return the updated user's profile in the response
      return updatedUser
    } catch (error: any | unknown) {
      // If an error occurs, abort the transaction and handle the error
      await session.abortTransaction();
      session.endSession();
      return res.status(500).json({ status: 'error', message: error.message || 'Failed to update profile picture' });
    }
  }
}

export default new UserService()