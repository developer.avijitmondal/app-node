import { Request, Response } from 'express';
import httpStatus from 'http-status';
import mongoose, { Types } from 'mongoose';
import QuestionServiceRepository from '../repository/questions.repository';
import multer from 'multer';
import csvParser from 'csv-parser';
import fs from 'fs';
import Question, { QuestionDocument } from '../models/question';

// Define CSV row structure
interface CSVRow {
  text: string;
  categories: string; // Comma-separated category IDs
}

export class QuestionService {

  async createQuestion(req: Request, res: Response) {
    return await QuestionServiceRepository.createQuestion(req, res)
  }

  async getQuestion(req: Request) {
    return await QuestionServiceRepository.getQuestion(req)
  }

  async bulkQuestion(req: Request, res: Response) {
    // Check if multer has thrown an error for invalid file type
    if ((req as any).fileValidationError) {
      return res.status(400).json({
        status: 'error',
        message: (req as any).fileValidationError.message || 'Invalid file type'
      });
    }

    // Check if req.file is defined using optional chaining
    const filePath = req.file?.path;

    if (!filePath) {
      return res.status(400).json({
        status: 'error',
        message: 'No file uploaded'
      });
    }

    // Read CSV file and parse data
    const results: CSVRow[] = [];
    fs.createReadStream(filePath)
      .pipe(csvParser())
      .on('data', (data: CSVRow) => results.push(data))
      .on('end', async () => {
        // Process each row from CSV
        const questions: QuestionDocument[] = [];
        // console.log(results)
        for (const result of results) {
          const { text, categories } = result;

          // Convert categories to array of ObjectId (assuming MongoDB ObjectId)
          const categoryIds = categories.split(',').map((categoryId) => categoryId.trim());

          // Create a new question for each category
          for (const categoryId of categoryIds) {
            // console.log(categoryId)
            const question = new Question({ text, categories: [categoryId] });
            await question.save();
            questions.push(question);
          }
        }

        // Cleanup: delete uploaded file
        fs.unlinkSync(filePath);

        return questions;
      });
  }
}

export default new QuestionService()