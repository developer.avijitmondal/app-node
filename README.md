# NodeJS Application

## Description

App Node is a NodeJS application designed to provide backend services for various functionalities including user management, category management, and question handling through a RESTful API. It includes features for user authentication, profile management, category creation, and question handling.

## Table of Contents

- [Getting Started](#getting-started)
  - [Clone the Repository](#clone-the-repository)
  - [Install Dependencies](#install-dependencies)
  - [Set up Environment Variables](#set-up-environment-variables)
  - [Start the Server](#start-the-server)
- [Usage](#usage)
  - [Endpoints](#endpoints)
    - [POST /users/profile-picture](#post-usersprofile-picture)
    - [GET /categories](#get-categories)
    - [POST /categories](#post-categories)
    - [GET /questions](#get-questions)
    - [POST /questions](#post-questions)
    - [POST /questions/bulk](#post-questionsbulk)
    - [POST /users/signup](#post-userssignup)
    - [POST /users/login](#post-userslogin)
    - [GET /users/profile](#get-usersprofile)
- [Integrations and Collaborations](#integrations-and-collaborations)
- [Swagger Documentation](#swagger-documentation)
- [Contributing](#contributing)
- [Support](#support)
- [License](#license)

## Getting Started

Follow these instructions to get a copy of the project up and running on your local machine for development and testing purposes.

### Clone the Repository

```bash
git clone https://gitlab.com/developer.avijitmondal/app-node.git
cd app-node
```
```bash
npm install
```

## Start development on TS

To start the express server, run the following

```bash
npm run dev
```

## To build the app

To start the express server, run the following

```bash
npm run build
```
Open [http://localhost:3000](http://localhost:3000) and take a look around.

## Open API documentions

Open [http://localhost:3000/api-docs/#/](http://localhost:3000/api-docs/#/) and take a look around.

